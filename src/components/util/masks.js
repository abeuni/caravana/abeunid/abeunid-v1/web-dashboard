export default {
  brazil: {
    documents: { cpf: '###.###.###-##', rg: '##.###.###-X' },
    address: { CEP: '#####-###' },
    phone: ['(##) ####-####', '(##) #####-####']
  }
}
